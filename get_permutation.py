def get_permutations(word):

    if len(word)<=1:
        return set(word)
    smaller = get_permutations(word[1:])
    perms = set()
    for x in smaller:
        for pos in range(0,len(x)+1):
            perm = x[:pos] + word[0] + x[pos:]
            perms.add(perm)
        return perms

print(get_permutations("nan"))
# {'nna', 'ann', 'nan'}
