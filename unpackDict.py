def likes(**kwargs):
    for key in kwargs:
        return(key + " likes " + kwargs[key])

likes(Alice="Bob", Bob="Ann", Ann="Alice")


# Alice likes Bob
# Bob likes Ann
# Ann likes Alice
