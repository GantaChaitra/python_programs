l = [[ ]]

if l:
    print(True)
else:
    print(False)
#Solution
#True

#Explanation
#In the line 'if l:' an implicit conversion of l to its boolean values happens.
#Assuming that l always points to a list, there are three possibilities what l can be: None, empty list, not empty list.
#None and empty list map to the boolean value False, only a not empty list maps to True.
#In the puzzle l is a list that contains one element, the empty list. Since l is not empty, it is converted to True. Thus the output is True.    


