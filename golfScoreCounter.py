player_list = ["double-bogey", "bogey", "triple-bogey", "bogey", "birdie"]
score_per_hole = [0,0,0,0,0]
list_pars = [2, 3, 5, 3, 4]
def par_scores(par, key):
    par_list = {"albatross" : -3, "eagle" : -2, "birdie" : -1, "bogey" : 1, "double-bogey" : 2, "triple-bogey" : 3}
    if key in par_list:
        par_total = par + par_list[key]
        return par_total
    return 0

def each_hole_score(list_pars, player_list):
    for i in range(0, len(player_list)):
        score_per_hole[i] = par_scores(list_pars[i] , player_list[i])
    return score_per_hole

total_score = sum(each_hole_score(list_pars, player_list))
total_par = sum(list_pars)

if(total_score > total_par):
    print("{} over {}".format(total_score - total_par, total_par))
else:
    print("{} under {}".format(total_par - total_score, total_par))
