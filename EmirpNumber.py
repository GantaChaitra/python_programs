###EMIPR NUMBERS :AN EMIRPIS A PRIME NUMBER THAT RESULTS IN A DIFFERENT PRIME WHEN ITS DECIMAL DIGITS ARE REVERSED . FOR EXAMPLE , 13 IS AN EMRIP NUMBER BECAUSE BOTH 13 AND 31 ARE PRIME NUMBERS.###



from sympy import isprime

def len_Palindrome(number):
    if(str(number) != str(number)[::-1] and len(str(number)) > 1):
        return True
    return False
    
def generate_EmirpNumbers(LIMIT):
    return [i for i in range(LIMIT) if(isprime(i) and isprime(int(str(i)[::-1])) and len_Palindrome(i))]
    
def check_EmirpNumber(number):
    if(isprime(number) and isprime(int(str(number)[::-1])) and len_Palindrome(number)):
        return True
    return False
    
    
print(check_EmirpNumber(151))
print(generate_EmirpNumbers(100))